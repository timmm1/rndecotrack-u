#!/bin/sh

v1=$(grep -Eow 'version="[0-9]+\.[0-9]+\.([0-9]+)"' config.xml | sed -E 's/version="[0-9]+\.[0-9]+\.([0-9]+)"/\1/')
v2=$((v1 + 1))
sed -i -E "s/(version=\"[0-9]+\.[0-9]+\.)$v1\"/\1$v2\"/" config.xml
rm platforms/android/app/build/outputs/apk/debug/*
cordova build android -- --gradleArg=-PcdvMinSdkVersion=26
cp -v platforms/android/app/build/outputs/apk/debug/app-debug.apk ~/Public/ecotrack-debug-v$v2.apk
