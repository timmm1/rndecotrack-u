/* global LocalFileSystem */

function errorHandler(e) {
    let msg = '';

    switch (e.code) {
        case FileError.QUOTA_EXCEEDED_ERR:
            msg = 'QUOTA_EXCEEDED_ERR';
            break;
        case FileError.NOT_FOUND_ERR:
            msg = 'NOT_FOUND_ERR';
            break;
        case FileError.SECURITY_ERR:
            msg = 'SECURITY_ERR';
            break;
        case FileError.INVALID_MODIFICATION_ERR:
            msg = 'INVALID_MODIFICATION_ERR';
            break;
        case FileError.INVALID_STATE_ERR:
            msg = 'INVALID_STATE_ERR';
            break;
        default:
            msg = 'Unknown Error';
            break;
    }
    alert('Filesystem Error: ' + msg);
}

function successHandler(fs) {
    console.log(fs);
    alert(cordova.file.dataDirectory);
}

export default {
    testFs() {
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, successHandler, errorHandler);
    }
};
