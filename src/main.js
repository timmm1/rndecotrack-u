import 'framework7/dist/css/framework7.css';
import 'framework7-icons/css/framework7-icons.css';
import 'font-awesome/css/font-awesome.css';
import 'material-design-icons/iconfont/material-icons.css';
import 'openlayers/css/ol.css';
import './assets/style.scss';

import Vue from 'vue';
import Framework7 from 'framework7/dist/framework7.esm.bundle';
import Framework7Vue from 'framework7-vue/dist/framework7-vue.esm.bundle';
import app from './main.vue';
import routes from './routes';
import store from './storage';
import OfflineHelper from './helpers/offline';

import axios from 'axios';
import config from 'config';
import InitDB from '@/db/init';

axios.defaults.baseURL = config.baseURL;
Vue.use(Framework7Vue, Framework7);

let theme = 'md';
if (document.location.search.indexOf('theme=') >= 0) {
    theme = document.location.search.split('theme=')[1].split('&')[0]; // eslint-disable-line
}

document.addEventListener('deviceready', onDeviceReady, false);

function poolLocation() {
    navigator.geolocation.getCurrentPosition(() => {}, () => {}, { enableHighAccuracy: true });
}

function onDeviceReady() {
    new Vue({
        // Root Element
        el: '#app',
        store,
        render: c => c('app'),
        components: {
            app,
        },
        framework7: {
            id: 'nz.ecotrack.mobile',
            theme, // md or ios
            dialog: { title: 'Ecotrack' },
            touch: { disableContextMenu: false }
        },
        routes,
    });
    InitDB.init();
    OfflineHelper.runSyncProccess();
    setInterval(poolLocation, 10 * 1e3);

    // mapper.download(sampleData, 15);
}

window.onerror = (msg, url, line, col, error) => {
    let extra = !col ? '' : '\ncolumn: ' + col; // eslint-disable-line
    extra += !error ? '' : '\nerror: ' + error; // eslint-disable-line
    alert(`Error: ${msg}\nLine: ${line} ${extra}`); // eslint-disable-line
};
