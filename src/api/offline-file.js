/* global FileTransfer, FileUploadOptions */
import axios from 'axios';

export default { save };

function getOptions(id, imageURI, token, remoteId) {
    let options = new FileUploadOptions();
    options.fileKey = 'file';
    options.chunkedMode = false;
    options.fileName = imageURI.substr(imageURI.lastIndexOf('/') + 1);
    options.mimeType = 'image/jpeg';
    options.params = { id, remoteId };
    options.headers = {
        Authorization: token,
        Connection: 'close'
    };
    return options;
}

function save(id, imageURI, token, remoteId) {
    return new Promise((resolve, reject) => {
        let ft = new FileTransfer();
        ft.upload(imageURI, `${axios.defaults.baseURL}/OfflineFile`, resolve, reject,
            getOptions(id, imageURI, token, remoteId));
    });
}
