import axios from 'axios';

export default {
    check(uuid, token) {
        let config = {
            params: { uuid },
            headers: { Authorization: token }
        };
        return axios.get('/stagedrecord', config)
            .then(response => response.data)
            .catch(error => { console.log(error); });
    }
};
