import Login from '@/components/loginpage';
import SignUp from '@/components/signup/signup';
import Profile from '@/components/profile';
import Disclaimer from '@/components/signup/disclaimer';
import SelectCode from '@/components/signup/select-code';
import Homepage from '@/components/homepage';
import LeftPanel from '@/components/panels/left-panel';
import RightPanel from '@/components/panels/right-panel';
import SearchResults from '@/components/search-results';
import Form from '@/components/form/form';
import Map from '@/components/map/map';
import DownloadTiles from '@/components/map/download-tiles';
import LoadingPage from '@/components/loadingpage';

import FormActions from '@/actions/form';
import RecordActions from '@/actions/record';

export default [
    {
        path: '/loadingpage',
        component: LoadingPage
    },
    {
        path: '/loginpage',
        component: Login
    },
    {
        path: '/profile',
        component: Profile
    },
    {
        path: '/disclaimer',
        component: Disclaimer
    },
    {
        path: '/signup',
        component: SignUp
    },
    {
        path: '/select-code',
        component: SelectCode
    },
    {
        path: '/',
        component: Homepage
    },
    {
        path: '/left-panel',
        component: LeftPanel
    },
    {
        path: '/right-panel',
        component: RightPanel
    },
    {
        path: '/search-results/:gallery/:form/:formId',
        component: SearchResults
    },
    {
        path: '/map',
        component: Map
    },
    {
        path: '/map-download',
        component: DownloadTiles
    },
    {
        path: '/form/:formId',
        async(routeTo, routeFrom, resolve, reject) {
            this.app.preloader.show();
            let { formId } = routeTo.params;
            let reference = routeTo.context;
            FormActions.load(formId)
                .then(viewmodel => {
                    if (reference) {
                        viewmodel.reference = JSON.parse(JSON.stringify(reference));
                    }
                    resolve({ component: Form }, { context: { viewmodel } });
                })
                .catch(err => {
                    console.log(err);
                    this.app.dialog.alert('Error loading data. Are you connected to the internet?');
                    reject();
                })
                .then(this.app.preloader.hide);
        }
    },
    {
        path: '/record/:recordId',
        async(routeTo, routeFrom, resolve, reject) {
            this.app.preloader.show();
            let { recordId } = routeTo.params;
            RecordActions.load(recordId)
                .then(viewmodel => {
                    resolve({ component: Form }, { context: { viewmodel } });
                })
                .catch(err => {
                    this.app.dialog.alert('Error loading data');
                    reject();
                })
                .then(this.app.preloader.hide);
        }
    },
    {
        path: '/offlinerecord/:recordId',
        async(routeTo, routeFrom, resolve, reject) {
            this.app.preloader.show();
            let { recordId } = routeTo.params;
            RecordActions.loadOffline(recordId)
                .then(viewmodel => {
                    resolve({ component: Form }, { context: { viewmodel } });
                })
                .catch(err => {
                    this.app.dialog.alert(
                        'This record has already been saved to server. ' +
                        'Please reload your search results');
                    reject();
                })
                .then(this.app.preloader.hide);
        }
    },
];
