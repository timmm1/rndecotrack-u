import axios from 'axios';

export default {
    get(latitude, longitude) {
        return axios.get('/neighborhoodpoints', { params: { latitude, longitude } })
            .then(response => response.data);
    },
};
