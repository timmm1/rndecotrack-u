import axios from 'axios';
import FormDB from '@/db/form';
import ViewModelHelper from '@/helpers/viewmodel';

export default {
    load(id) {
        return new Promise((resolve, reject) => {
            axios.get('/form', { params: { id } })
                .then(response => {
                    ViewModelHelper.runOnLoadTasks(response.data);
                    // ViewModelHelper.loadDefaultValues(response.data);
                    FormDB.save(id, JSON.stringify(response.data));
                    resolve(response.data);
                })
                .catch(reject);
        });
    },
};
