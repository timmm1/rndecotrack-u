import InitDB from './init';

const db = InitDB.getDB;

export default {
    save(username, password, initialData) {
        db().then(cn => {
            cn.transaction(tx => {
                tx.executeSql('DELETE FROM login WHERE username = ?', [username]);
                const insertSQL = 'INSERT INTO login(username, password, initialData) VALUES (?, ?, ?)';
                tx.executeSql(insertSQL, [username, password, JSON.stringify(initialData)]);
            });
        });
    },

    login(username, password) {
        return new Promise((resolve, reject) => {
            db().then(cn => {
                cn.transaction(tx => {
                    const sql = 'SELECT initialData FROM login WHERE username = ? AND password = ?';
                    tx.executeSql(sql, [username, password], (tx1, results) => {
                        if (results.rows.length === 1) {
                            let json = JSON.parse(results.rows.item(0).initialData);
                            resolve(json);
                        } else {
                            reject();
                        }
                    });
                });
            });
        });
    }
};
