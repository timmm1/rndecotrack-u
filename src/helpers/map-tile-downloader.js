/* global FileTransfer */
import Store from '@/storage';

const servers = [
    'https://a.tile.openstreetmap.org/',
    'https://b.tile.openstreetmap.org/',
    'https://c.tile.openstreetmap.org/',
];

function download(polygon, zoom) {
    let bbox = getBoundariesBox(polygon);
    alert(JSON.stringify(bbox));
    let tileCoords = calcMinAndMaxValues(bbox, zoom);
    let downloadList = getTiles(tileCoords, zoom);
    Store.dispatch('startMapDownload', downloadList.length);
    downloadFiles(downloadList, 0);
}

function downloadFiles(downloadList, index) {
    Store.dispatch('mapDownloadProgress', index);
    if (index < downloadList.length) {
        let item = downloadList[index];

        let file = '{z}/{x}/{y}.png'
            .replace('{z}', item.z)
            .replace('{x}', item.x)
            .replace('{y}', item.y);

        let localUri = `${cordova.file.externalCacheDirectory}tiles/${file}`;

        let next = () => { downloadFiles(downloadList, index + 1); };

        window.resolveLocalFileSystemURL(
            localUri,
            next,
            () => {
                let uri = servers[item.server] + file;
                let fileTransfer = new FileTransfer();
                fileTransfer.download(uri, localUri, next, next);
            });
    }
}

function getTiles(tileCoords, zoom) {
    let i = 0;
    let downloadList = [];
    for (let x = tileCoords.xMin; x < tileCoords.xMax; x++) {
        for (let y = tileCoords.yMin; y < tileCoords.yMax; y++) {
            downloadList.push({
                server: i % 3,
                z: zoom,
                x,
                y });
            i++;
        }
    }
    return downloadList;
}

export default { download };

function getBoundariesBox(polygon) {
    let p1 = polygon.replace('POLYGON ((', '').replace('))', '');
    let p2 = p1.split(',');
    let p3 = p2.map(x => x.trim().split(' ').map(y => parseFloat(y)));
    let longs = p3.map(x => x[0]);
    let lats = p3.map(x => x[1]);
    return {
        north: Math.max(...lats),
        south: Math.min(...lats),
        east: Math.max(...longs),
        west: Math.min(...longs)
    };
}

// Given a bounding box and zoom level, calculate x and y tile ranges
function calcMinAndMaxValues(bbox, zoom) {
    return {
        yMin: lat2tile(bbox.north, zoom),
        yMax: lat2tile(bbox.south, zoom),
        xMin: long2tile(bbox.west, zoom),
        xMax: long2tile(bbox.east, zoom)
    };
}

function long2tile(lon, zoom) {
    return (Math.floor((lon + 180) / 360 * Math.pow(2, zoom))); // eslint-disable-line
}
function lat2tile(lat, zoom) {
    return (Math.floor((1 - Math.log(Math.tan(lat * Math.PI / 180) + 1 / Math.cos(lat * Math.PI / 180)) / Math.PI) / 2 * Math.pow(2, zoom))); // eslint-disable-line
}
