import axios from 'axios';
import InitDB from './init';

let db = InitDB.getDB;

export default {
    save(recordId, uri) {
        return new Promise((resolve, reject) => {
            db().then(cn => {
                cn.transaction(tx => {
                    const id = Date.now();
                    const insertSQL = 'INSERT INTO files(id, token, recordId, uri) VALUES (?, ?, ?, ?)';
                    const token = axios.defaults.headers.common['Authorization'];
                    tx.executeSql(insertSQL, [id, token, recordId, uri], resolve, reject);
                });
            });
        });
    },

    delete(id) {
        db().then(cn => {
            cn.transaction(tx => {
                tx.executeSql('DELETE FROM files WHERE id = ?', [id]);
            });
        });
    },

    getAll() {
        return new Promise(resolve => {
            db().then(cn => {
                cn.transaction(tx => {
                    const sql = 'SELECT * FROM files';
                    tx.executeSql(sql, [], (tx1, results) => {
                        resolve(results.rows);
                    });
                });
            });
        });
    }
};
