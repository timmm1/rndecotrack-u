import config from 'config';

const CREATE_LOGIN_TABLE = 'CREATE TABLE login (username, password, initialData)';
const CREATE_FORMS_TABLE = 'CREATE TABLE forms (id, json)';
const CREATE_FILES_TABLE = 'CREATE TABLE files (id, token, recordId, uri)';
const CREATE_RECORDS_TABLE = 'CREATE TABLE records (id, token, json, staged)';
const MIGRATION_1_TO_2_ADD_COLUMN_TO_RECORDS = 'ALTER TABLE records ADD staged';

let DB = null;
let dbReady = () => {
    DB = openDatabase(config.dbName, config.dbVersion, null, config.dbSize);
};

export default {
    init() {
        const db = openDatabase(config.dbName, '', null, config.dbSize);

        if (db.version === '') {
            db.changeVersion('', '2', tx => {
                tx.executeSql(CREATE_LOGIN_TABLE);
                tx.executeSql(CREATE_FORMS_TABLE);
                tx.executeSql(CREATE_FILES_TABLE);
                tx.executeSql(CREATE_RECORDS_TABLE);
            }, () => alert('database error: 1'), dbReady);
        }
        if (db.version === '1.0') {
            alert('upgrading from db v1 to v2');
            db.changeVersion('1.0', '2', tx => {
                tx.executeSql(MIGRATION_1_TO_2_ADD_COLUMN_TO_RECORDS);
            }, () => alert('database error: 2'), dbReady);
        }
        if (db.version === '2') {
            dbReady();
        }
    },

    getDB() {
        return new Promise((resolve, reject) => {
            if (DB !== null) {
                resolve(DB);
            } else {
                reject(new Error('Database is not ready'));
            }
        });
    }
};

// VERSION 1.0
// const CREATE_RECORD_TABLE_SQL =
//     'CREATE TABLE IF NOT EXISTS records (id, token, json)';

// const db = openDatabase('agilecloud_db', '1.0', '', 5*1024*1024);
// sql = 'sELECT * FROM X'
// db.transaction(tx => {
//     tx.executeSql(sql, [], (tx1, results) => {
//         console.log(results);
//     });
// });
