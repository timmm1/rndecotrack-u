import FormDB from '@/db/form';
import FormAPI from '@/api/form';
import ViewModelHelper from '@/helpers/viewmodel';

export default {
    load(id) {
        return new Promise((resolve, reject) => {
            FormDB.load(id)
                .then(formJson => {
                    FormAPI.load(id);
                    let form = JSON.parse(formJson);
                    ViewModelHelper.loadDefaultValues(form);
                    resolve(form);
                })
                .catch(() => {
                    FormAPI.load(id)
                        .then(form => {
                            ViewModelHelper.loadDefaultValues(form);
                            resolve(form);
                        })
                        .catch(reject);
                });
        });
    },
};
