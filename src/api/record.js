import axios from 'axios';
import ViewModelHelper from '@/helpers/viewmodel';

export default {
    load(id) {
        return axios.get('/record', { params: { id } })
            .then(response => ViewModelHelper.runOnLoadTasks(response.data));
    },

    save(viewmodel) {
        let saveData = ViewModelHelper.convertToSaveFormat(viewmodel);

        return new Promise((resolve, reject) => {
            axios.post('/record', saveData)
                .then(response => {
                    if (response.data.form) {
                        ViewModelHelper.runOnLoadTasks(response.data.form);
                    }
                    resolve(response.data);
                })
                .catch(reject);
        });
    }
};
