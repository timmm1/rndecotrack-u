#!/bin/bash

password="P^(URM:up~2E=2>)sjEx<Vw.^6w[2pWb"
signedfile="app-release-signed.apk"

cordova build --release android -- --gradleArg=-PcdvMinSdkVersion=26
cp -v platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk ../deploys
# cp -v platforms/android/build/outputs/apk/android-release-unsigned.apk ../deploys

cd ../deploys

jarsigner -verbose \
	  -sigalg SHA1withRSA \
	  -digestalg SHA1 \
	  -keystore ~/Trabalho/AgileCloud/PlayStore/agilecloud.keystore \
	  -storepass $password \
	  app-release-unsigned.apk \
	  AgileCloud

[ -e $signedfile ] && rm $signedfile

zipalign -v 4 app-release-unsigned.apk $signedfile
