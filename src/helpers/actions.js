/* global cordova */
import Vue from 'vue';
import ViewModelHelper from './viewmodel';
import RecordActions from '@/actions/record';
import FileDB from '@/db/file';

export default {
    save(form) {
        if (validateSave(form)) {
            form.$f7.preloader.show();

            form.viewmodel.recordName = ViewModelHelper.createFormName(form.viewmodel);
            form.viewmodel.reference = null;

            let success = false;

            RecordActions.save(form.viewmodel)
                .then(response => {
                    if (response.offline) {
                        success = true;
                        form.viewmodel.id = response.id;
                        form.mode = 'view';
                        if (form.viewmodel.onsave) {
                            form.viewmodel.onsave(response, true);
                        }
                    } else {
                        success = response.success;
                        if (response.success) {
                            if (form.viewmodel.onsave) {
                                form.viewmodel.onsave(response.form);
                            }
                            form.viewmodel = response.form;
                            form.mode = 'view';
                        } else {
                            form.$f7.dialog.alert(response.message);
                        }
                    }
                })
                .catch(error => {
                    form.$f7.dialog.alert(`Error saving data: ${error.message}`);
                })
                .then(() => {
                    if (success) {
                        form.$f7.dialog.alert('Record Saved');
                    }
                    form.$f7.preloader.hide();
                });
        }
    },

    refresh(form) {
        form.$f7.preloader.show();
        RecordActions.load(form.viewmodel.id)
            .then(response => {
                form.viewmodel = response;
                form.$f7.preloader.hide();
            });
    },

    getLocation(form) {
        let lat = ViewModelHelper.findControl(form.viewmodel, c => c.name === 'Latitude');
        let long = ViewModelHelper.findControl(form.viewmodel, c => c.name === 'Longitude');

        if (form.$device.desktop) {
            console.log('--- DESKTOP ---');
            lat.value = -36.86667;
            long.value = 174.76667;
            return;
        }
        /*
         * cordova.plugins.diagnostic.isGpsLocationEnabled is available only for Android
         */
        let isLocationAvailable =
            cordova.plugins.diagnostic.isGpsLocationEnabled ?
                cordova.plugins.diagnostic.isGpsLocationEnabled :
                cordova.plugins.diagnostic.isLocationAvailable;

        isLocationAvailable(enabled => {
            function onSuccess(position) {
                lat.value = position.coords.latitude;
                long.value = position.coords.longitude;
                form.$f7.dialog.alert('GPS coordinates added. Please remember to save the record.');
            }

            function onError(error) {
                alert('code: ' + error.code + '\nmessage: ' + error.message + '\n'); // eslint-disable-line
            }

            if (enabled) {
                navigator.geolocation.getCurrentPosition(onSuccess, onError, { enableHighAccuracy: true });
            } else {
                form.$f7.dialog.alert('Please enable GPS');
            }
        }, error => {
            alert(error); // eslint-disable-line
        });
    },

    getPicture(form) {
        function getPictureSuccess(imageURI) {
            const { viewmodel } = form;
            let filename = imageURI.substr(imageURI.lastIndexOf('/') + 1);

            let control = ViewModelHelper.findControl(viewmodel, c => c.name === 'FileName');
            if (control) {
                control.value = filename;
            }
            let attachment = {
                filename,
                uri: imageURI,
                date: Date.now()
            };
            if (!viewmodel.attachments) {
                Vue.set(viewmodel, 'attachments', [attachment]);
            } else {
                viewmodel.attachments.push(attachment);
            }
            FileDB.save(viewmodel.id, imageURI);
            form.$f7.dialog.alert('Photo Saved');
        }

        function getPictureError(message) {
            alert(`Error: ${message}`); // eslint-disable-line
        }

        navigator.camera.getPicture(getPictureSuccess, getPictureError, { quality: 100 });
    },
};

function validateSave(form) {
    let flag = false;
    form.viewmodel.tabs.forEach(tab => {
        tab.controls.forEach(control => {
            if (control.required) {
                if (!(control.value || control.value === false || control.value === 0)) {
                    flag = true;
                }
            }
        });
    });
    if (flag) {
        form.showRequired = true;
        form.$f7.dialog.alert('Some required fields are empty. They have been highlighted.');
    }
    return !flag;
}
