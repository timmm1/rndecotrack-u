import axios from 'axios';

export default {
    load(gallery, form, formId, query) {
        return new Promise((resolve, reject) => {
            axios.get('/preflight')
                .then(() => {

                    axios.get('/gallery', { params: { gallery, form, formId, query } })
                        .then(response => { resolve(response.data); })
                        .catch(() => reject(new Error('Error loading data from server')));

                })
                .catch(() => {
                    reject(new Error("Can't load remote records. Are you online?"));
                });
        });
    }
};
