/* global FileTransferError */
import AccountDB from '@/db/account';
import RecordDB from '@/db/record';
import FileDB from '@/db/file';
import FormAPI from '@/api/form';
import OfflineRecordAPI from '@/api/offline-record';
import OfflineFileAPI from '@/api/offline-file';
import StagedRecordAPI from '@/api/staged-record';
import moment from 'moment';

let UPLOADING_FILES = [];

export default {

    loginProccess(username, password, initialData) {
        AccountDB.save(username, password, initialData);
        preloadGalleries(initialData);
    },

    runSyncProccess() {
        let sync = () => {
            RecordDB.getUnsynced().then(rows => {
                for (let i = 0; i < rows.length; i++) {
                    let item = rows.item(i);
                    let { token } = item;
                    let viewmodel = JSON.parse(item.json);
                    viewmodel.timestamp = item.id;
                    OfflineRecordAPI.save(viewmodel, token)
                        .then(response => {
                            if (response) {
                                RecordDB.markSynced(item.id, response);
                            }
                        });
                }
            });
            RecordDB.getSynced().then(rows => {
                for (let i = 0; i < rows.length; i++) {
                    let item = rows.item(i);
                    let { token, staged } = item;
                    StagedRecordAPI.check(item.id, token)
                        .then(response => {
                            if (response) {
                                let stagedDate = moment(staged);
                                let lastUpdate = moment(response);
                                if (lastUpdate.isAfter(stagedDate)) {
                                    console.log('LAST UPDATE IS AFTER: ' + item.id);
                                }
                                console.log(stagedDate.format());
                                console.log(lastUpdate.format());
                                RecordDB.delete(item.id);
                            }
                        });
                }
            });
            FileDB.getAll().then(rows => {
                for (let i = 0; i < rows.length; i++) {
                    let item = rows.item(i);

                    if (!UPLOADING_FILES.includes(item.id)) {
                        UPLOADING_FILES.push(item.id);
                        let { recordId, token, uri, id: remoteId } = item;
                        OfflineFileAPI.save(recordId, uri, token, remoteId)
                            .then(() => {
                                FileDB.delete(item.id);
                            })
                            .catch(error => {
                                UPLOADING_FILES = UPLOADING_FILES.filter(id => id !== item.id);
                                if (error.code === FileTransferError.FILE_NOT_FOUND_ERR) {
                                    FileDB.delete(item.id);
                                }
                                // alert(`File upload #${recordId} failed with error: ${error.code}`);
                            });
                    }

                }
            });
        };
        setInterval(sync, 30 * 1e3);
    }
};

function preloadGalleries(initialData) {
    let galleries = initialData.gallery;
    galleries.forEach(gallery => {
        gallery.items.forEach(item => {
            FormAPI.load(item.formId);
        });
    });
}
