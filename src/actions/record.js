import RecordDB from '@/db/record';
import RecordAPI from '@/api/record';

export default {
    load(id) {
        return RecordAPI.load(id);
    },

    loadOffline(id) {
        return RecordDB.load(id);
    },

    save(viewmodel) {
        return new Promise((resolve, reject) => {
            RecordDB.save(viewmodel)
                .then(dbId => {
                    resolve({ id: dbId, offline: true });
                })
                .catch(reject);
        });
    }
};
