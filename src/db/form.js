import InitDB from './init';

let db = InitDB.getDB;

export default {
    save(id, form) {
        db().then(cn => {
            cn.transaction(tx => {
                tx.executeSql('DELETE FROM forms WHERE id = ?', [id]);
                const insertSQL = 'INSERT INTO forms(id, json) VALUES (?, ?)';
                tx.executeSql(insertSQL, [id, form]);
            });
        });
    },

    load(id) {
        return new Promise((resolve, reject) => {
            db().then(cn => {
                cn.transaction(tx => {
                    const sql = 'SELECT json FROM forms WHERE id = ?';
                    tx.executeSql(sql, [id], (tx1, results) => {
                        if (results.rows.length === 1) {
                            let { json } = results.rows.item(0);
                            resolve(json);
                        } else {
                            reject();
                        }
                    });
                });
            });
        });
    }
};
