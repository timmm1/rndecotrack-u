# README #

Agile Cloud Mobile app using Apache Cordova, Framework7 and VueJS.

When cloning the repository for the first time:

    # cordova platform add browser

If you're using Windows, you also need install the dev packages:

    # npm install

To compile and run the project:

    # cordova run browser -- --live-reload


If you want to keep the Chrome profile folder between browser restarts, you can set its path on file
`platforms/browser/cordova/node_modules/cordova-serve/src/browser.js`:

    // ...
    function getBrowser(target, dataDir) {
        var chromeArgs = ' --user-data-dir=/home/giuseppe/.cordova/agilecloud_chrome_profile';
        // ...
