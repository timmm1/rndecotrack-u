function getCurrentPosition() {
    return new Promise((resolve, reject) => {
        let isLocationAvailable =
            cordova.plugins.diagnostic.isGpsLocationEnabled ?
                cordova.plugins.diagnostic.isGpsLocationEnabled :
                cordova.plugins.diagnostic.isLocationAvailable;

        function onError(error) {
            alert('code: ' + error.code + '\nmessage: ' + error.message + '\n');
            reject(error);
        }
        isLocationAvailable(enabled => {
            if (enabled) {
                navigator.geolocation.getCurrentPosition(resolve, onError, { enableHighAccuracy: true });
            } else {
                this.$f7.dialog.alert('Please enable GPS');
            }
        }, error => {
            alert(error); // eslint-disable-line
        });
    });
}

function calculateDistance(coord1, coord2) {
    let R = 6371e3; // metres
    let φ1 = toRadians(coord1.latitude);
    let φ2 = toRadians(coord2.latitude);
    let Δφ = toRadians(coord2.latitude - coord1.latitude);
    let Δλ = toRadians(coord2.longitude - coord1.longitude);
    let a = (Math.sin(Δφ / 2) * Math.sin(Δφ / 2)) + (Math.cos(φ1) * Math.cos(φ2) * Math.sin(Δλ / 2) * Math.sin(Δλ / 2));
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    let d = R * c;
    return d;
}

function toRadians(degrees) {
    return degrees * (Math.PI / 180);
}

function parseCoordinates(data) {
    return data
        .replace('MULTIPOLYGON', '')
        .replace('POLYGON', '')
        .replace('LINESTRING', '')
        .replace(/\(/g, '')
        .replace(/\)/g, '')
        .split(',')
        .map(item =>
            item.split(' ')
                .map(parseFloat)
                .filter(Number));
}

export default {
    getCurrentPosition,
    calculateDistance,
    parseCoordinates
};
