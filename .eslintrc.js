// http://eslint.org/docs/user-guide/configuring

module.exports = {
    globals: {
        cordova: true
    },
    env: {
        browser: true,
    },
    extends: 'airbnb-base',
    // required to lint *.vue files
    plugins: [
        'html'
    ],
    // check if imports actually resolve
    settings: {
        'html/html-extensions': ['.html', '.vue'],
        'import/resolver': {
            webpack: {
                config: './webpack.config.js'
            }
        }
    },
    // add your custom rules here
    rules: {
        'no-new': 'off',
        'no-alert': 'off',
        'eol-last': 'off',
        'no-console': 'off',
        'func-names': 'off',
        'no-plusplus': 'off',
        'arrow-parens': 'off',
        'comma-dangle': 'off',
        'dot-notation': 'off',
        'prefer-const': 'off',
        'import/first': 'off',
        'default-case': 'off',
        'prefer-const': 'off',
        'no-lonely-if': 'off',
        'no-loop-func': 'off',
        'padded-blocks': 'off',
        'no-cond-assign': 'off',
        'spaced-comment': 'off',
        'global-require': 'off',
        'prefer-template': 'off',
        'no-param-reassign': 'off',
        'no-trailing-spaces': 'off',
        'object-curly-newline': 'off',
        'function-paren-newline': 'off',
        'no-multiple-empty-lines': 'off',
        'space-before-function-paren': 'off',

        semi: 'warn',
        quotes: 'warn',
        indent: ['warn', 4, { SwitchCase: 1 }],
        'max-len': 'off', // ['warn', 120],
        'no-empty': 'warn',
        'key-spacing': 'warn',
        'arrow-spacing': 'warn',
        'block-spacing': 'warn',
        'no-unreachable': 'warn',
        'no-multi-spaces': 'warn',
        'object-shorthand': 'warn',
        'arrow-body-style': 'warn',
        'object-curly-spacing': 'warn',
        'prefer-arrow-callback': 'warn',
        'no-unused-vars': ['warn', {
            args: 'none',
            ignoreRestSiblings: true
        }],
        'brace-style': ['warn', '1tbs', {
            allowSingleLine: true
        }],
        'prefer-destructuring': ['warn', {
            AssignmentExpression: {
                array: true,
                object: false
            }
        }],
        'no-use-before-define': ['error', {
            functions: false,
            classes: true
        }],
        // don't require .vue extension when importing
        'import/extensions': ['error', 'always', {
            js: 'never',
            vue: 'never'
        }],
        // allow optionalDependencies
        'import/no-extraneous-dependencies': ['error', {
            optionalDependencies: ['test/unit/index.js']
        }],
        // allow debugger during development
        'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0
    }
};
