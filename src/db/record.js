import axios from 'axios';
import InitDB from './init';
import Store from '@/storage';

const db = InitDB.getDB;

function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
    let r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8); // eslint-disable-line
        return v.toString(16);
    });
}

export default {
    save(record) {
        return new Promise(resolve => {
            db().then(cn => {
                cn.transaction(tx => {
                    record.id = record.id === '0' ? uuidv4() : record.id;
                    tx.executeSql('DELETE FROM records WHERE id = ?', [record.id], () => {
                        const insertSQL = 'INSERT INTO records(id, token, json) VALUES (?, ?, ?)';
                        const token = axios.defaults.headers.common['Authorization'];
                        tx.executeSql(insertSQL, [record.id.toString(), token, JSON.stringify(record)],
                            () => { console.log('record saved successfuly'); },
                            (tx1, error) => { console.log(error); });

                        Store.dispatch('savedRecord', record);
                        resolve(record.id);
                    });
                });
            });
        });
    },

    load(id) {
        return new Promise((resolve, reject) => {
            db().then(cn => {
                cn.transaction(tx => {
                    const sql = 'SELECT * FROM records WHERE id = ?';
                    tx.executeSql(sql, [id.toString()], (tx1, results) => {
                        if (results.rows.length === 1) {
                            let item = results.rows.item(0);
                            let viewmodel = JSON.parse(item.json);
                            resolve(viewmodel);
                        } else {
                            reject();
                        }
                    });
                });
            });
        });
    },

    count() {
        return new Promise(resolve => {
            db().then(cn => {
                cn.transaction(tx => {
                    const sql = 'SELECT COUNT(id) FROM records';
                    tx.executeSql(sql, [], (tx1, results) => {
                        if (results && results.rows && results.rows[0]) {
                            let count = results.rows[0]['COUNT(id)'];
                            resolve(count);
                        } else {
                            resolve(0);
                        }
                    });
                });
            });
        });
    },

    delete(id) {
        console.log('Deleting: ' + id);
        db().then(cn => {
            cn.transaction(tx => {
                tx.executeSql('DELETE FROM records WHERE id = ?', [id.toString()]);
            });
        });
    },

    markSynced(id, stagedTimestamp) {
        console.log('Mark synced: ' + id);
        db().then(cn => {
            cn.transaction(tx => {
                tx.executeSql('UPDATE records SET staged = ? WHERE id = ?', [stagedTimestamp, id.toString()]);
            });
        });
    },

    getSynced() {
        return new Promise(resolve => {
            db().then(cn => {
                cn.transaction(tx => {
                    const sql = 'SELECT * FROM records WHERE staged IS NOT NULL';
                    tx.executeSql(sql, [], (tx1, results) => {
                        resolve(results.rows);
                    });
                });
            });
        });
    },

    getUnsynced() {
        return new Promise(resolve => {
            db().then(cn => {
                cn.transaction(tx => {
                    const sql = 'SELECT * FROM records WHERE staged IS NULL';
                    tx.executeSql(sql, [], (tx1, results) => {
                        resolve(results.rows);
                    });
                });
            });
        });
    },

    getAll() {
        return new Promise(resolve => {
            db().then(cn => {
                cn.transaction(tx => {
                    const sql = 'SELECT * FROM records';
                    tx.executeSql(sql, [], (tx1, results) => {
                        resolve(results.rows);
                    });
                });
            });
        });
    }
};
