import axios from 'axios';
import AccountDB from '@/db/account';
import OfflineHelper from '@/helpers/offline';

export default {
    login(username, password) {
        return new Promise((resolve, reject) => {
            axios.get('/preflight')
                .then(() => {
                    axios.get('/account', { params: { username, password } })
                        .then(response => {
                            if (response.data.success) {
                                axios.defaults.headers.common['Authorization'] = response.data.token;
                                OfflineHelper.loginProccess(username, password, response.data);
                            }
                            resolve(response.data);
                        })
                        .catch(() => { reject(new Error('Invalid username or password')); });

                })
                .catch(() => {
                    AccountDB.login(username, password)
                        .then(initialData => {
                            axios.defaults.headers.common['Authorization'] = initialData.token;
                            resolve(initialData);
                        })
                        .catch(() => {
                            reject(new Error('Invalid username or password. Are you connected to the internet?'));
                        });
                });
        });
    },
    resetPassword(username) {
        const encoded = encodeURI(username);
        const url = `/resetpassword?username=${encoded}`;

        return new Promise((resolve, reject) => {
            axios.post(url)
                .then(response => {
                    const { data } = response;
                    if (data.success) {
                        resolve();
                    } else if (data.message) {
                        reject(data.message);
                    } else {
                        reject(new Error('Server error'));
                    }
                })
                .catch(() => {
                    reject(new Error('Error connecting to the server'));
                });
        });
    },
    signUp(data) {
        return new Promise((resolve, reject) => {
            axios.post('/signup', data)
                .then(response => {
                    resolve(response.data);
                })
                .catch(reject);
        });
    },
    getCommunityGroups() {
        return new Promise((resolve, reject) => {
            axios.get('/CommunityGroups')
                .then(response => {
                    resolve(response.data);
                })
                .catch(reject);
        });
    }
};
