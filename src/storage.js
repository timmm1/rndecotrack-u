import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        references: [],
        token: null,
        galleries: [],
        icons: [],
        displayLeftPanel: false,
        lastSavedRecord: null,
        mapMenuItems: null,
        map: {
            totalFiles: 0,
            downloaded: 0
        }
    },
    getters: {
        getLastSavedRecord: state => () => state.lastSavedRecord
    },

    actions: {
        userLogged({ commit }, logonData) {
            commit('USER_LOGGED', logonData);
        },
        startMapDownload({ commit }, totalFiles) {
            commit('START_MAP_DOWNLOAD', totalFiles);
        },
        mapDownloadProgress({ commit }, downloaded) {
            commit('MAP_DOWNLOAD_PROGRESS', downloaded);
        },
        loadReferences({ commit }, references) {
            commit('LOAD_REFERENCES', references);
        },
        savedRecord({ commit }, record) {
            commit('SAVED_RECORD', record);
        }
    },

    mutations: {
        USER_LOGGED(state, logonData) {
            state.token = logonData.token;
            state.icons = logonData.icons;
            state.galleries = logonData.gallery;
            state.mapMenuItems = logonData.mapMenu;
            state.displayLeftPanel = logonData.showMenu;
        },
        START_MAP_DOWNLOAD(state, totalFiles) {
            state.map.totalFiles = totalFiles;
            state.map.downloaded = 0;
        },
        MAP_DOWNLOAD_PROGRESS(state, downloaded) {
            state.map.downloaded = downloaded;
        },
        LOAD_REFERENCES(state, references) {
            state.references = references;
        },
        SAVED_RECORD(state, record) {
            state.lastSavedRecord = record;
        }
    },
});
