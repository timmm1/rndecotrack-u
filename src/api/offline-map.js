import axios from 'axios';

export default {
    get() {
        return axios.get('/offlineMap').then(response => response.data);
    }
};
