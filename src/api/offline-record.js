import axios from 'axios';
import ViewModelHelper from '@/helpers/viewmodel';

export default {
    save(viewmodel, token) {
        let config = {
            headers: { Authorization: token }
        };

        let saveData = ViewModelHelper.convertToSaveFormat(viewmodel);
        // console.log('-- Offline record: ');
        // console.log(saveData);

        return axios.post('/offlinerecord', saveData, config)
            .then(response => response.data)
            .catch(error => { console.log(error); });
    }
};
