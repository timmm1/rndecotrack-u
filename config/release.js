module.exports = {
    // baseURL: 'http://192.168.1.121:45455/api',
    baseURL: 'https://agilecloud.com/mobile/1.2.74/api',
    markerURL: 'https://agilecloud.com/mobile/1.2.74/assets/marker',
    imageURL: 'https://www.agilecloud.com/assets/images',

    dbName: 'agilecloud_db',
    dbVersion: '2',
    dbSize: 5 * 1024 * 1024
};
