import ActionHelper from './actions';
import Store from '@/storage';

export default {

    runAction(form, button) {
        switch (button.action) {
            case 'Save':
                ActionHelper.save(form);
                break;

            case 'Edit':
                form.mode = 'edit';
                break;

            case 'Refresh':
                ActionHelper.refresh(form);
                break;

            case 'Cancel':
                form.mode = 'view';
                break;

            case 'GetLocation':
                ActionHelper.getLocation(form);
                break;

            case 'TakePhoto':
                ActionHelper.getPicture(form);
                break;

            case 'AddReference':
                button.detail.form = form;
                Store.dispatch('loadReference', button.detail);
                form.$f7.panel.open('right');
                // form.$emit('show-references', button.detail);
                break;

            default:
                form.$f7.dialog.alert(`Action "${button.action}" is not yet implemented`);
                break;
        }
    },

    getButtonIcon(action) {
        let icon;
        switch (action) {
            case 'Edit':
            case 'Cancel':
            case 'Refresh':
                icon = action.toLowerCase();
                break;

            case 'New':
                icon = 'add';
                break;

            case 'GetLocation':
                icon = 'place';
                break;

            case 'TakePhoto':
                icon = 'camera_alt';
                break;

            case 'AddReference':
                icon = 'more_vert';
                break;

            default:
                icon = 'help_outline';
                break;
        }
        return icon;
    }
};
