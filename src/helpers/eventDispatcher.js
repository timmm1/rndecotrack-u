let actionRunner = {};

export default {
    dispatch(actions, viewmodel) {
        actions.forEach(action => {
            if (actionRunner[action.targetType]) {
                actionRunner[action.targetType](action, viewmodel);
            }
        });
    }
};

actionRunner['tab'] = (action, viewmodel) => {
    const tab = viewmodel.tabs.find(t => t.id === action.targetId);
    switch (action.action) {
        case 'Hide':
            tab.hidden = true;
            break;
    }
};
