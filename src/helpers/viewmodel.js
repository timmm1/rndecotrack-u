import libMoment from 'moment';
import EventDispatcher from './eventDispatcher';

function findControl(viewmodel, predicate) {
    for (let i = 0; i < viewmodel.tabs.length; i++) {
        let found = viewmodel.tabs[i].controls.find(predicate);
        if (found) return found;
    }
    return null;
}

export default {
    findControl,

    runOnLoadTasks(viewmodel) {
        fixCheckboxValues(viewmodel);
        if (viewmodel.onLoad) {
            EventDispatcher.dispatch(viewmodel.onLoad, viewmodel);
        }
        return viewmodel;
    },

    convertToSaveFormat(viewmodel) {
        const { id, timestamp, schema, formName } = viewmodel;
        const tabs = [];
        viewmodel.tabs.forEach(tab => tabs.push({ controls: tab.controls }));
        const saveData = { id, timestamp, schema, formName, tabs };
        return saveData;
    },

    createFormName(viewmodel) {
        const regex = /\[(\w+)\]/g;
        let newName = viewmodel.nameFormula;
        let match;
        while (match = regex.exec(viewmodel.nameFormula)) {
            let control = findControl(viewmodel, c => c.name === match[1]);
            if (control) {
                newName = newName.replace(match[0], control.value);
            }
        }
        return newName;
    },

    loadDefaultValues(viewmodel) {
        viewmodel.tabs.forEach(tab => {
            tab.controls.forEach(control => {
                if (control.defaultValue) {
                    if (control.type === 'Role') {
                        [control.value, control.thingId] = control.defaultValue.split('|');
                    } else {
                        let matches = control.defaultValue.match(/^javascript:\((.*)\)$/);
                        if (Array.isArray(matches) && matches[1]) {
                            try {
                                let result = sandbox.call({}, matches[1]);
                                control.value = result;
                            } catch (e) {
                                alert('Error evaluating DefaultValue'); // eslint-disable-line no-alert
                            }
                        } else {
                            control.value = control.defaultValue;
                        }
                    }
                }
            });
        });
        return viewmodel;
    }
};

function fixCheckboxValues(viewmodel) {
    viewmodel.tabs.forEach(tab => {
        tab.controls.forEach(control => {
            if (control.type === 'CheckBox') {
                control.value = parseInt(control.value, 10);
            }
        });
    });
    return viewmodel;
}

function sandbox(str) {
    // ### Declare sandbox variables
    // webpack messes with the name of the import, so we
    // need to rebind the variable to the name we want
    const moment = libMoment; // eslint-disable-line no-unused-vars
    // ## Evaluate string
    return eval(str); // eslint-disable-line no-eval
}
